# Agile 
Agile就是dotnet core 基础类库<br/>

使用开发框架的好处：<br/>
1.框架在技术上为软件系统提供了完整的模式实践<br/>
2.框架为团队提供了合理可行的软件开发过程模式<br/>
3.框架的应用大大提高了团队的开发效率，团队只需要关注与领域相关的业务实现，而无需关注具体的技术实现<br/>
4.框架的应用大大降低了出现缺陷（Bug）的几率，因为大多数支撑业务系统的代码都经过了严格的测试和实战的考验<br/>
5.框架的应用还为软件系统的整合与集成带来了便捷<br/>















#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
