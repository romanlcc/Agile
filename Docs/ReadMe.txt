领域驱动设计
Repository 和 UnitOfWork 共享一个 IDbContext 对象
方案一：
从 Repository 和 UnitOfWork 中抽离出 IDbContext，并且它们只依赖于 IDbContext
Repository 和 UnitOfWork 为平级关系
UnitOfWork 负责维护对象状态（增删改）
Repository 负责获取对象（查）
方案二：
Repository 和 UnitOfWork 还是依赖于 IDbContext
UnitOfWork 只有 Commit，Repository 提供对象的所有操作（增删改查）

事件总线（Event Bus）知多少 https://mp.weixin.qq.com/s/OWQSRCl3xu5TlEdKLBe7qA https://github.com/sheng-jie/EventBus


    <PackageReference Include="MiniProfiler.AspNetCore.Mvc" Version="4.1.0" />
	
	
系统监控：
用户轨迹
数据审计
异常日志
健康日志


del /S /Q "$(SolutionDir)\Agile.WebHost\Modules\$(ProjectName)\"

xcopy  "$(TargetDir)*.*" "$(SolutionDir)\Agile.WebHost\Modules\$(ProjectName)\"  /S /Y /C /E

xcopy  "$(ProjectDir)\wwwroot\*.*" "$(SolutionDir)\Agile.WebHost\Modules\$(ProjectName)\wwwroot\"  /S /Y /C /E

del /S /Q "$(SolutionDir)\Agile.WebHost\Modules\$(ProjectName)\*.pdb"




    <!--生成NuGet包
    <IsPackable>true</IsPackable>
    <PackageOutputPath>..\..\..\_packages</PackageOutputPath>
    <PackageId>$(MSBuildProjectName)</PackageId>
    <AssemblyName>$(MSBuildProjectName)</AssemblyName>
    <Product>$(MSBuildProjectName)</Product>
    <Description>asp.net core development class library</Description>
    <releaseNotes>Agile封装了Auth,AutoMapper,Cache,Comm,Config,Data,Host,Module,Options,Quartz,Swagger,Tencent等常用开发类,可以方便的调用。</releaseNotes>
    <RepositoryType>git</RepositoryType>
    <tags>agile;asp.net core;.net core;efcore;Auth;AutoMapper;Cache;Comm;Config;Data;Host;Module;Options;Quartz;Swagger;Tencent</tags>
    <PackageTags>agile;asp.net core;.net core;efcore;</PackageTags>
    <licenseUrl>http://www.apache.org/licenses/LICENSE-2.0.html</licenseUrl>
    <projectUrl>https://gitee.com/luomingui/projects</projectUrl>
    <iconUrl>https://blog-static.cnblogs.com/files/luomingui/luo.ico</iconUrl>
    <PackageProjectUrl>https://gitee.com/luomingui/projects</PackageProjectUrl>
    <RepositoryUrl>https://gitee.com/luomingui/projects</RepositoryUrl>
    -->



















