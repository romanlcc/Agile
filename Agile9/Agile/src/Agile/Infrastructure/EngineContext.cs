﻿using System;
using System.Runtime.CompilerServices;

using Microsoft.AspNetCore.Http;

namespace Agile
{
    public class EngineContext
    {
        #region Methods
        [MethodImpl(MethodImplOptions.Synchronized)]
        public static IEngine Create()
        {
            return Singleton<IEngine>.Instance ?? (Singleton<IEngine>.Instance = new AgileEngine());
        }
        public static void Replace(IEngine engine)
        {
            Singleton<IEngine>.Instance = engine;
        }

        #endregion

        #region Properties
        public static IEngine Current
        {
            get
            {
                if (Singleton<IEngine>.Instance == null)
                {
                    Create();
                }
                return Singleton<IEngine>.Instance;
            }
        }
        #endregion
    }

    public static class HttpContextCore
    {
        public static HttpContext Current { get => EngineContext.Current.Resolve<IHttpContextAccessor>().HttpContext; }
    }
}
