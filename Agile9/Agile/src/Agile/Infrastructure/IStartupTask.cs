﻿namespace Agile
{
    public interface IStartupTask
    {
        void Execute();
        int Order { get; }
    }
}
