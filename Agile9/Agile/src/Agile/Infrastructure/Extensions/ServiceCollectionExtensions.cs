﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Agile
{
    public static partial class ServiceCollectionExtensions
    {
        public static IServiceProvider AddAgileCore(this IServiceCollection services, IConfiguration configuration, IHostEnvironment env)
        {
            //create default file provider
            Utils.DefaultFileProvider = new AgileFileProvider(env);
            Utils.ContentRootPath = env.ContentRootPath;

            AgileEngine.LicenseContext = LicenseContext.NonCommercial;
            //create engine and configure service provider
            var engine = EngineContext.Create();
            var serviceProvider = engine.ConfigureServices(services, configuration);

            return serviceProvider;
        }
        public static void UseAgileCore(this IApplicationBuilder app)
        {
            EngineContext.Current.ConfigureRequestPipeline(app);
        }
    }
}
