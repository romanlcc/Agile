using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Agile
{
    public class AgileMvcStartup : IAgileStartup
    {
        public int Order => 7000;

        public void Configure(IApplicationBuilder app)
        {

        }

        public void ConfigureMvc(MvcOptions mvcOptions)
        {

        }

        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddMemoryCache();
            services.AddOptions();
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<ILoginInfo, LoginInfo>();
            services.AddSingleton<VerifyCode>();


            
        }
    }
}
