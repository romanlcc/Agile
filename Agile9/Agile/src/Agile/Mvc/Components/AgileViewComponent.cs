﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewComponents;

namespace Microsoft.AspNetCore.Mvc
{

    public abstract class AgileViewComponent : ViewComponent
    {
        private void PublishModelPrepared<TModel>(TModel model)
        {

        }
        public new ViewViewComponentResult View<TModel>(string viewName, TModel model)
        {
            PublishModelPrepared(model);
            return base.View<TModel>(viewName, model);
        }
        public new ViewViewComponentResult View<TModel>(TModel model)
        {
            PublishModelPrepared(model);
            return base.View<TModel>(model);
        }
        public new ViewViewComponentResult View(string viewName)
        {
            return base.View(viewName);
        }
    }
}
