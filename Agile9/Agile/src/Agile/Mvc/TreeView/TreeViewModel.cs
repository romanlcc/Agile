﻿/*******************************************************************************
 * Copyright © 2016 NFine.Framework 版权所有
 * Author: NFine
 * Description: NFine快速开发平台
 * Website：http://www.nfine.cn
*********************************************************************************/
using System.Diagnostics;

namespace System
{
    [DebuggerDisplay(" text:{text}, value:{value},id:{id}")]
    public class TreeViewModel
    {
        public string parentId { get; set; }
        /// <summary>
        /// node id
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// 用于显示的节点文本
        /// </summary>
        public string text { get; set; }
        /// <summary>
        /// 节点值
        /// </summary>
        public string value { get; set; }
        /// <summary>
        /// 复选框检查状态。 0表示未选中，1表示部分选中，2表示已选中。
        /// </summary>
        public int? checkstate { get; set; }
        /// <summary>
        /// 是否显示复选框
        /// </summary>
        public bool showcheck { get; set; }
        /// <summary>
        /// See hasChildren.
        /// </summary>
        public bool complete { get; set; }
        /// <summary>
        /// 展开或折叠
        /// </summary>
        public bool isexpand { get; set; }
        /// <summary>
        /// 如果hasChildren并且complete设置为true，
        /// 并且ChildNodes为空，则tree将请求服务器获取子节点。
        /// </summary>
        public bool hasChildren { get; set; }
        public string img { get; set; }
        public string title { get; set; }
    }
}
