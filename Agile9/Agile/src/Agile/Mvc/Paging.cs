﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace System
{
    /// <summary>
    /// jqgrid分页
    /// </summary>
    public class Paging
    {
        public Paging()
        {
            keyword = "";
            page = 1;
            rows = 10;
            sidx = "ID";
            sord = "asc";
            field = "*";
            sqlwhere = "1=1";
        }
        public string keyword { get; set; }
        /// <summary>
        /// 本页记录数
        /// </summary>
        public int rows { get; set; }
        /// <summary>
        /// 当前页
        /// </summary>
        public int page { get; set; }
        public string sidx { get; set; }
        public string sord { get; set; }
        public string field { get; set; }
        public string sqlwhere { get; set; }
        public string orderby
        {
            get
            {
                return sidx + " " + sord;
            }
        }
        /// <summary>
        /// 总页数
        /// </summary>
        public int total { get; set; }
        /// <summary>
        /// 总记录数
        /// </summary>
        public int totalCount { get; set; }
        public int GetSkip()
        {
            return (page - 1) * rows;
        }
        public static string GetjqGridJson(string RowsJson, int total, int page, int totalCount)
        {
            return "{\"rows\":[" + RowsJson + "],\"total\":\"" + total + "\",\"page\":\"" + page + "\",\"records\":\"" + totalCount + "\"}";
        }
        public static string GetjqGridJson(string RowsJson, int page, int totalCount)
        {
            int total = (page - 1) * totalCount;
            return "{\"rows\":[" + RowsJson + "],\"total\":\"" + total + "\",\"page\":\"" + page + "\",\"records\":\"" + totalCount + "\"}";
        }
        public static string GetjqGridJson(string RowsJson)
        {
            return "{\"rows\":[" + RowsJson + "]}";
        }
    }
    /// <summary>
    /// jqrid data
    /// </summary>
    public class JQridPageData
    {
        public JQridPageData()
        {
            pageRows = 10;
        }
        /// <summary>
        /// 数据
        /// </summary>
        public object rows { get; set; }
        /// <summary>
        /// 当前页
        /// </summary>
        public int page { get; set; }
        /// <summary>
        /// 每页显示记录数
        /// </summary>
        public int pageRows { get; set; }
        /// <summary>
        ///总页数
        /// </summary>
        public int total
        {
            get
            {
                int _total = 1;
                try
                {
                    _total = records % pageRows > 0 ? records / pageRows + 1 : records / pageRows;
                }
                catch
                {
                    _total = 1;
                }
                return _total;
            }
        }
        /// <summary>
        /// 总记录数
        /// </summary>
        public int records { get; set; }
    }
}
