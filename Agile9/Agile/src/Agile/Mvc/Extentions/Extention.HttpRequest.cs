﻿using System;
using System.IO;
using System.Linq;

namespace Microsoft.AspNetCore.Http
{
    /// <summary>
    /// 拓展类
    /// </summary>
    public static partial class Extention
    {
        public static bool IsAjaxRequest(this HttpRequest req)
        {
            bool result = false;
            var xreq = req.Headers.ContainsKey("x-requested-with");
            if (xreq)
            {
                result = req.Headers["x-requested-with"] == "XMLHttpRequest";
            }
            return result;
        }
        public static Stream GetPluginStream(this HttpRequest request)
        {
            if (request == null || request.Form.Files.Count == 0)
            {
                throw new Exception("The plugin package is missing.");
            }
            return request.Form.Files.First().OpenReadStream();
        }
    }
}
