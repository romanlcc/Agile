﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;

namespace Microsoft.AspNetCore.Http
{
    /// <summary>
    /// 拓展类
    /// </summary>
    public static partial class Extention
    {
        public static string MapPath(this HttpContext httpContext, string virtualPath)
        {
            IActionContextAccessor actionContext = new ActionContextAccessor();
            UrlHelper urlHelper = new UrlHelper(actionContext.ActionContext);
            virtualPath = urlHelper.Content(virtualPath);
            
            return $"{Path.Combine(new List<string> { actionContext.ActionContext.HttpContext.MapPath("") }.Concat(virtualPath.Split('/')).ToArray())}";
        }
    }
}
