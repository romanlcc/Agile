﻿using System;
using System.Collections.Generic;
using System.Web;
using Agile;

namespace Microsoft.AspNetCore.Mvc
{
    /// <summary>
    /// Mvc基控制器
    /// Index 
    /// Form 
    /// Details 
    /// </summary>
    public class BaseController : Controller
    {
        #region Index Form Details Success Error
        [HttpGet]
        public virtual IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public virtual IActionResult Form()
        {
            return View();
        }
        [HttpGet]
        public virtual IActionResult Details()
        {
            return View();
        }

        protected virtual IActionResult Success(string message = "操作成功")
        {
            return Content(new Result { Success=true, StatusCode = 200, Message = message }.ToJson());
        }
        protected virtual IActionResult Success(string message, object data)
        {
            return Content(new Result<object> { Success = true, StatusCode = 200, Message = message, Data = data }.ToJson());
        }
        protected virtual IActionResult Error(string message = "操作失败")
        {
            return Content(new Result { Success = false, StatusCode = 500, Message = message }.ToJson());
        }
        #endregion

        #region Service
        private readonly Dictionary<Type, object> _services = new Dictionary<Type, object>();
        /// <summary>
        /// 获取服务。
        /// </summary>
        public TService Service<TService>() where TService : class
        {
            if (!_services.ContainsKey(typeof(TService)))
            {
                _services[typeof(TService)] = EngineContext.Current.Resolve<TService>();
            }
            return (TService)_services[typeof(TService)];
        }
        #endregion

        /// <summary>
        /// 当前URL是否包含某字符串
        /// 注：忽略大小写
        /// </summary>
        /// <param name="subUrl">包含的字符串</param>
        /// <returns></returns>
        public bool UrlContains(string subUrl)
        {
            return Request.Path.ToString().ToLower().Contains(subUrl.ToLower());
        }
        /// <summary>
        /// 导出Excel
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        protected IActionResult ExportExcel(string filePath, string fileName)
        {
            if (fileName.IsNullOrEmpty())
            {
                fileName = DateTime.Now.ToString("yyyyMMddHHmmss");
            }
            return PhysicalFile(filePath, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", HttpUtility.UrlEncode(fileName), true);
        }
        /// <summary>
        /// 获取cookies
        /// </summary>
        /// <param name="key">键</param>
        /// <returns>返回对应的值</returns>
        protected string GetCookies(string key)
        {
            HttpContext.Request.Cookies.TryGetValue(key, out string value);
            if (string.IsNullOrEmpty(value)) value = "";
            return value;
        }
    }
}
