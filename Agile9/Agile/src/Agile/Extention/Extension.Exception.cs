﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System
{
    public static partial class Extention
    {
        public static string GetInnerException(this Exception ex)
        {
            if (ex.InnerException != null)
            {
                return GetInnerException(ex.InnerException);
            }
            return ex.Message;
        }
    }
}
