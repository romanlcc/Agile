﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace System
{
    [DebuggerDisplay("Key:{Key}, Value:{Value.ToString()}")]
    public class KeyValue
    {
        public KeyValue()
        {
            
        }
        public KeyValue(string key,object value)
        {
            Key = key;
            Value = value;
        }
        /// <summary>
        /// 键KEY
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// 键值
        /// </summary>
        public object Value { get; set; }
    }
}
