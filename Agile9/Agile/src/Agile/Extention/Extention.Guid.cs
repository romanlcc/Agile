﻿using System;

namespace System
{
    public static partial class Extention
    {
        /// <summary>
        /// 转为有序的GUID
        /// 注：长度为50字符
        /// </summary>
        /// <param name="guid">新的GUID</param>
        /// <returns></returns>
        public static string ToSequentialGuid(this Guid guid)
        {
            return GuidHelper.NewSequentialGuid().ToString();
        }
        /// <summary>
        /// 转小写
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static string ToLower(this Guid guid)
        {
            return guid.ToString().ToLower();
        }
        /// <summary>
        /// 判断Guid是否为空
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool IsEmpty(this Guid s)
        {
            return s == Guid.Empty;
        }

        /// <summary>
        /// 判断Guid是否不为空
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool NotEmpty(this Guid s)
        {
            return s != Guid.Empty;
        }
    }
}
