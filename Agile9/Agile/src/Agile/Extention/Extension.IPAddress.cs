﻿using System.Net;

namespace System
{
    /// <summary>
    /// IPAddress 扩展操作类
    /// </summary>
    public static partial class Extention
    {
        /// <summary>
        /// 将 IPAddress 转换为 IPv4 格式字符串形式
        /// </summary>
        /// <param name="address">IPAddress 实例</param>
        /// <returns>字符串形式</returns>
        public static string ToIPv4String(this IPAddress address)
        {
            var ipv4Address = (address ?? IPAddress.IPv6Loopback).ToString();
            return ipv4Address.StartsWith("::ffff:") ? address.MapToIPv4().ToString() : ipv4Address;
        }
    }
}
