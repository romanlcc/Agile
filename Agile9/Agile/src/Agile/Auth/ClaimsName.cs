﻿using System.IO;

namespace System
{
    /// <summary>
    /// Claims名称
    /// </summary>
    public static class ClaimsName
    {
        public const string ThemeId = "ThemeId";
        public const string TenantId = "TenantId";
        public const string Phone = "Phone";
        public const string Email = "Email";
        public const string UserID = "UserID";
        public const string UserName = "UserName";
        public const string AccountType = "AccountType";
        public const string LoginTime = "LoginTime";
        /// <summary>
        /// 平台 如 PC IOS.....
        /// </summary>
        public const string Platform = "Platform";
     
    }
}
