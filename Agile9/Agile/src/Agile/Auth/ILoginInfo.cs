﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Http;

namespace System
{
    /// <summary>
    /// 登录信息
    /// </summary>
    public interface ILoginInfo
    {
        /// <summary>
        /// 租户
        /// </summary>
        string TenantId { get; }
        /// <summary>
        /// 账户编号
        /// </summary>
        string AccountId { get; }
        /// <summary>
        /// 账户名称
        /// </summary>
        string AccountName { get; }
        /// <summary>
        /// 获取当前用户IP(包含IPv和IPv6)
        /// </summary>
        string IP { get; }
        /// <summary>
        /// 登录时间戳
        /// </summary>
        DateTime LoginTime { get; }
        Platform Platform { get; }
    }
    /// <summary>
    /// 登录信息
    /// </summary>
    public class LoginInfo : ILoginInfo
    {
        private readonly IHttpContextAccessor _contextAccessor;
        public LoginInfo(IHttpContextAccessor contextAccessor)
        {
            _contextAccessor = contextAccessor;
        }
        /// <summary>
        /// 租户
        /// </summary>
        public string TenantId
        {
            get
            {
                try
                {
                    var tenantId = _contextAccessor?.HttpContext?.User?.FindFirst(ClaimsName.TenantId);
                    if (tenantId != null && tenantId.Value.NotNull())
                    {
                        return tenantId.Value;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("ILoginInfo TenantId " + ex.Message);
                }
                return string.Empty;
            }
        }
        /// <summary>
        /// 账户编号
        /// </summary>
        public string AccountId
        {
            get
            {
                try
                {
                    var accountId = _contextAccessor?.HttpContext?.User?.FindFirst(ClaimsName.UserID);
                    if (accountId != null && accountId.Value.NotNull())
                    {
                        return accountId.Value;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("ILoginInfo AccountId " + ex.Message);
                }
                return string.Empty;
            }
        }
        public string AccountName
        {
            get
            {
                try
                {
                    var accountName = _contextAccessor?.HttpContext?.User?.FindFirst(ClaimsName.UserName);
                    if (accountName != null && accountName.Value.NotNull())
                    {
                        return accountName.Value;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("ILoginInfo AccountName " + ex.Message);
                }
                return string.Empty;
            }
        }
        public string Phone
        {
            get
            {
                try
                {
                    var claim = _contextAccessor?.HttpContext?.User?.FindFirst(ClaimsName.Phone);
                    if (claim != null && claim.Value.NotNull())
                    {
                        return claim.Value;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("ILoginInfo Phone " + ex.Message);
                }
                return string.Empty;
            }
        }
        public string Email
        {
            get
            {
                try
                {
                    var claim = _contextAccessor?.HttpContext?.User?.FindFirst(ClaimsName.Email);
                    if (claim != null && claim.Value.NotNull())
                    {
                        return claim.Value;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("ILoginInfo Email " + ex.Message);
                }
                return string.Empty;
            }
        }
        /// <summary>
        /// 获取当前用户IP(包含IPv和IPv6)
        /// </summary>
        public string IP
        {
            get
            {
                try
                {
                    return _contextAccessor?.HttpContext?.Connection?.RemoteIpAddress?.ToString();
                }
                catch { }
                return "127.0.0.1";
            }
        }
        /// <summary>
        /// 登录时间
        /// </summary>
        public DateTime LoginTime
        {
            get
            {
                try
                {
                    var loginTime = _contextAccessor?.HttpContext?.User?.FindFirst(ClaimsName.LoginTime);
                    if (loginTime != null && loginTime.Value.NotNull())
                    {
                        return loginTime.Value.ToDateTime();
                    }
                }
                catch
                {
                }
                return DateTime.Now;
            }
        }
        /// <summary>
        /// 登录平台
        /// </summary>
        public Platform Platform
        {
            get
            {
                try
                {
                    var platform = _contextAccessor?.HttpContext?.User?.FindFirst(ClaimsName.Platform);
                    if (platform != null && platform.Value.NotNull())
                    {
                        return (Platform)platform.Value.ToInt();
                    }
                }
                catch
                {
                }
                return Platform.Web;
            }
        }
    }
}