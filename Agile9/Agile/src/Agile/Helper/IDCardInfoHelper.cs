﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace System
{
    /// <summary>
    /// 根据身份证获取身份证信息
    /// </summary>
    public class IDCardInfoHelper
    {
        /// <summary>
        /// 根据身份证获取身份证信息
        /// 18位身份证
        /// 0地区代码(1~6位,其中1、2位数为各省级政府的代码，3、4位数为地、市级政府的代码，5、6位数为县、区级政府代码)
        /// 1出生年月日(7~14位)
        /// 2顺序号(15~17位单数为男性分配码，双数为女性分配码)
        /// 3性别
        ///
        /// 15位身份证
        /// 0地区代码
        /// 1出生年份(7~8位年,9~10位为出生月份，11~12位为出生日期
        /// 2顺序号(13~15位)，并能够判断性别，奇数为男，偶数为女
        /// 3性别
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public static string[] GetIdCardInfo(string cardId)
        {
            string[] info = new string[4];

            if (string.IsNullOrEmpty(cardId))
            {
                return info;
            }

            try
            {
                Regex regex = null;
                if (cardId.Length == 18)
                {
                    regex = new Regex(@"^\d{17}(\d|x|X)$");
                    if (regex.IsMatch(cardId))
                    {

                        info.SetValue(cardId.Substring(0, 6), 0);
                        info.SetValue(DateTime.ParseExact(cardId.Substring(6, 8), "yyyyMMdd", CultureInfo.CurrentCulture).ToString("yyyy-MM-dd"), 1);
                        info.SetValue(cardId.Substring(14, 3), 2);
                        info.SetValue(Convert.ToInt32(info[2]) % 2 != 0 ? "男" : "女", 3);
                    }
                }
                else if (cardId.Length == 15)
                {
                    regex = new Regex(@"^\d{15}");
                    if (regex.IsMatch(cardId))
                    {
                        info.SetValue(cardId.Substring(0, 6), 0);
                        info.SetValue(DateTime.ParseExact(cardId.Substring(6, 6), "yyyyMMdd", CultureInfo.CurrentCulture).ToString("yyyy-MM-dd"), 1);
                        info.SetValue(cardId.Substring(12, 3), 2);
                        info.SetValue(Convert.ToInt32(info[2]) % 2 != 0 ? "男" : "女", 3);
                    }
                }
            }
            catch (Exception ex)
            {
                info[0] = ex.Message;
            }
            return info;
        }
        /// <summary>
        /// 生肖(根据生日自动计算)
        /// </summary>
        public static string GetZodiac(DateTime birthday)
        {
            int year = birthday.Year;
            string[] shuxiang = { "鼠", "牛", "虎", "兔", "龙", "蛇", "马", "羊", "猴", "鸡", "狗", "猪" };
            int tmp = year - 2008;
            if (year < 2008)
            {
                return shuxiang[tmp % 12 + 12];
            }
            else
            {
                return shuxiang[tmp % 12];
            }
        }
        /// <summary>
        ///  星座(根据生日自动计算)
        /// </summary>
        public static string GetConstellation(DateTime birthday)
        {
            float birthdayF = 0.00F;

            if (birthday.Month == 1 && birthday.Day < 20)
            {
                birthdayF = float.Parse(string.Format("13.{0}", birthday.Day));
            }
            else
            {
                birthdayF = float.Parse(string.Format("{0}.{1}", birthday.Month, birthday.Day));
            }
            float[] atomBound = { 1.20F, 2.20F, 3.21F, 4.21F, 5.21F, 6.22F, 7.23F, 8.23F, 9.23F, 10.23F, 11.21F, 12.22F, 13.20F };
            string[] atoms = { "水瓶座", "双鱼座", "白羊座", "金牛座", "双子座", "巨蟹座", "狮子座", "处女座", "天秤座", "天蝎座", "射手座", "魔羯座" };

            string ret = "靠！外星人啊。";
            for (int i = 0; i < atomBound.Length - 1; i++)
            {
                if (atomBound[i] <= birthdayF && atomBound[i + 1] > birthdayF)
                {
                    ret = atoms[i];
                    break;
                }
            }
            return ret;
        }
    }
}
