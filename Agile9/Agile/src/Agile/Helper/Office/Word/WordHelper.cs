﻿using System;
using System.IO;

namespace System
{
    /// <summary>
    /// 何学裕
    /// 2018-01-16
    /// 操作word
    /// </summary>
    public class WordHelper
    {
        //#region 替换word中的内容
        ////调用实例
        ////string oldstr = "{name},{branch}";
        ////string newstr = "张三，人事处";
        ////WordHelper.ReplceWordText(oldstr.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries),
        ////newstr.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)
        ////       , "/upfile/合同模版/合同模版.docx", "/upfile/合同模版/张三劳动合同.docx");

        ///// <summary>
        ///// 读取出word文档中的内容后对word文档进行修改并保存指定目录 
        ///// </summary>
        ///// <param name="oldstr">原字符,多处替换以逗号分隔(如：{name},{branch})</param>
        ///// <param name="newstr">替换新字符,多处替换以逗号分隔(如：张三，人事处)</param>
        ///// <param name="pathfile">文件地址（如：/upfile/old.docx）</param>
        ///// <param name="downfile">替换后文件地址（如：/upfile/new.docx）</param>
        //public static void ReplceWordText(string[] oldstr, string[] newstr, string pathfile,string downfile)
        //{
        //    Microsoft.Office.Interop.Word.Document oWordDoc = null;
        //    Microsoft.Office.Interop.Word.ApplicationClass oWordApp = null;
        //    object missing = System.Reflection.Missing.Value;
        //    try
        //    {
        //        object fileName = AppConfig.hostingEnvironment + pathfile;
        //        object readOnly = false;
        //        object isVisible = true;
        //        oWordApp = new Microsoft.Office.Interop.Word.ApplicationClass();
        //        oWordDoc = oWordApp.Documents.Open(ref fileName, ref missing, ref readOnly,
        //                             ref missing, ref missing, ref missing, ref missing, ref missing, ref missing,
        //                             ref missing, ref missing, ref isVisible, ref missing, ref missing, ref missing);
        //        #region 字符替换

        //        for (int i = 0; i < oldstr.Length; i++)
        //        {
        //            oWordApp = Replce(oldstr[i], newstr[i], oWordApp);
        //        }
        //        #endregion

        //        #region 保存文件到客户端
        //        string filePath = AppConfig.hostingEnvironment + downfile;

        //        filePath = filePath.Substring(0, filePath.LastIndexOf("/"));
                
        //        if (!Directory.Exists(filePath))
        //        {
        //            Directory.CreateDirectory(filePath);
        //        }
        //        //filename = filePath + "/" + filename;
        //        if (!System.IO.File.Exists(AppConfig.hostingEnvironment + downfile))
        //        {
        //            oWordDoc.SaveAs(AppConfig.hostingEnvironment + downfile);
        //            //ClientScript.RegisterStartupScript(ClientScript.GetType(), "myscript", @"<script>alert('合同文档生成成功，已保存在本地磁盘D:\\WordDown\\" + jtmodel.name.Trim() + ".doc！');</script>");
        //        }
        //        else
        //        {
        //            //ClientScript.RegisterStartupScript(ClientScript.GetType(), "myscript", @"<script>alert('该合同文档已经存在本地磁盘D:\\WordDown\\" + jtmodel.name.Trim() + ".doc，生成失败！');</script>");
        //        }
        //        oWordApp.NormalTemplate.Saved = true;
        //        oWordDoc.Close(ref missing, ref missing, ref missing);
        //        oWordApp.Application.Quit(ref missing, ref missing, ref missing);
        //        #endregion
        //    }
        //    catch(Exception ee)
        //    {
        //        oWordDoc.Close(ref missing, ref missing, ref missing);
        //        oWordApp.Application.Quit(ref missing, ref missing, ref missing);
        //    }
        //}


        ///// <summary>
        ///// 替换word中的字符串
        ///// </summary>
        ///// <param name="oldstr">被替换的符</param>
        ///// <param name="newstr">替换后的字符</param>
        ///// <param name="oWordApp"></param>
        //private static Microsoft.Office.Interop.Word.ApplicationClass Replce(string oldstr, string newstr, Microsoft.Office.Interop.Word.ApplicationClass oWordApp)
        //{
        //    object missing = System.Reflection.Missing.Value;
        //    oWordApp.Selection.Find.ClearFormatting();
        //    oWordApp.Selection.Find.Replacement.ClearFormatting();
        //    oWordApp.Selection.Find.Text = oldstr;
        //    oWordApp.Selection.Find.Replacement.Text = newstr;
        //    object objReplace = Microsoft.Office.Interop.Word.WdReplace.wdReplaceAll;
        //    oWordApp.Selection.Find.Execute(ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing,
        //        ref missing, ref missing, ref missing, ref objReplace, ref missing, ref missing, ref missing, ref missing);
        //    return oWordApp;
        //}

        //#endregion
    }
}