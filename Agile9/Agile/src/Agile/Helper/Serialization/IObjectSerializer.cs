﻿using System;

namespace System.Serialization
{
    public interface IObjectSerializer
    {
        byte[] Serialize<TObject>(TObject obj);
        TObject Deserialize<TObject>(byte[] stream);
    }
}
