﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace System.Serialization {
    public enum SerializerType
    {
        Binary,
        DataContract,
        JSON,
        XML
    }
    public class SerializerFactory
    {
        public static IObjectSerializer Factory(SerializerType type = SerializerType.JSON)
        {
            IObjectSerializer iobjectSerializer = null;
            switch (type)
            {
                case SerializerType.Binary:
                    iobjectSerializer = new ObjectBinarySerializer();
                    break;
                case SerializerType.DataContract:
                    iobjectSerializer = new ObjectDataContractSerializer();
                    break;
                case SerializerType.JSON:
                    iobjectSerializer = new ObjectJsonSerializer();
                    break;
                case SerializerType.XML:
                    iobjectSerializer = new ObjectXmlSerializer();
                    break;
            }
            return iobjectSerializer;
        }
    }
}
