﻿using System;
using System.IO;
using NSXmlSerialization = System.Xml.Serialization;

namespace System.Serialization {
    public class ObjectXmlSerializer : IObjectSerializer
    {
        #region IObjectSerializer Members
        public virtual byte[] Serialize<TObject>(TObject obj)
        {
            Type graphType = obj.GetType();
            NSXmlSerialization.XmlSerializer xmlSerializer = new NSXmlSerialization.XmlSerializer(graphType);
            byte[] ret = null;
            using (MemoryStream ms = new MemoryStream())
            {
                xmlSerializer.Serialize(ms, obj);
                ret = ms.ToArray();
                ms.Close();
            }
            return ret;
        }
        public virtual TObject Deserialize<TObject>(byte[] stream)
        {
            NSXmlSerialization.XmlSerializer xmlSerializer = new NSXmlSerialization.XmlSerializer(typeof(TObject));
            using (MemoryStream ms = new MemoryStream(stream))
            {
                TObject ret = (TObject)xmlSerializer.Deserialize(ms);
                ms.Close();
                return ret;
            }
        }
        #endregion

    }
    /// <summary>
    /// ObjectXmlSerializerEx 的摘要说明。
    /// </summary>
    public static class ObjectXmlSerializerEx
    {
        /// <summary>
        /// 反序列化 ObjectXmlSerializerEx
        /// </summary>
        /// <param name="type">对象类型</param>
        /// <param name="filename">文件路径</param>
        /// <returns></returns>
        public static object Load(this ObjectXmlSerializer oxs, Type type, string filename)
        {
            FileStream fs = null;
            try
            {
                // open the stream...
                fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                NSXmlSerialization.XmlSerializer serializer = new NSXmlSerialization.XmlSerializer(type);
                return serializer.Deserialize(fs);
            }
            catch
            {
                return null;
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        /// <summary>
        /// 序列化 ObjectXmlSerializerEx
        /// </summary>
        /// <param name="obj">对象</param>
        /// <param name="filename">文件路径</param>
        public static bool Save(this ObjectXmlSerializer oxs, object obj, string filename)
        {
            bool success = false;

            FileStream fs = null;
            // serialize it...
            try
            {
                fs = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
                NSXmlSerialization.XmlSerializer serializer = new NSXmlSerialization.XmlSerializer(obj.GetType());
                serializer.Serialize(fs, obj);
                success = true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
            return success;

        }

        public static string ModelToXml<TObject>(this ObjectXmlSerializer oxs, TObject model)
        {
            var bytes = oxs.Serialize<TObject>(model);
            MemoryStream stream = new MemoryStream(bytes);
            stream.Position = 0;
            StreamReader sr = new StreamReader(stream);
            return sr.ReadToEnd();
        }
        public static TObject XmlToModel<TObject>(this ObjectXmlSerializer oxs, string xml)
        {
            StringReader xmlReader = new StringReader(xml);
            NSXmlSerialization.XmlSerializer xmlSer = new NSXmlSerialization.XmlSerializer(typeof(TObject));
            return (TObject)xmlSer.Deserialize(xmlReader);
        }
    }
}
