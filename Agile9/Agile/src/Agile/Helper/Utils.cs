﻿using System;
using System.Collections;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
namespace System
{
    /// <summary>
    /// 工具类
    /// </summary>
    public partial class Utils
    {
       
        /// <summary>
        /// 获得当前绝对路径
        /// </summary>
        /// <param name="strPath">指定的路径</param>
        /// <returns>绝对路径</returns>
        public static string GetMapPath(string strPath)
        {
            strPath = strPath.Replace("/", "\\").Replace("~/", "").Replace("~\\", "");
            if (strPath.StartsWith("\\"))
            {
                strPath = strPath.Substring(strPath.IndexOf('\\', 1)).TrimStart('\\');
            }
            return System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, strPath);
            
        }
        /// <summary>
        /// 把数组转为逗号连接的字符串
        /// </summary>
        /// <param name="data"></param>
        /// <param name="Str"></param>
        /// <returns></returns>
        public static string ArrayToString(dynamic data, string Str)
        {
            string resStr = Str;
            foreach (var item in data)
            {
                if (resStr != "")
                {
                    resStr += ",";
                }

                if (item is string)
                {
                    resStr += item;
                }
                else
                {
                    resStr += item.Value;

                }
            }
            return resStr;
        }

        /// <summary>
        /// 将传入的字符串中间部分字符替换成特殊字符
        /// <code>
        /// ReplaceWithSpecialChar("柯小呆", 1, 0,'*') -->Result: 柯*呆
        /// ReplaceWithSpecialChar("622212345678485") -->Result: 6222*******8485
        /// ReplaceWithSpecialChar("622212345678485", 4 , 4 , '*') -->Result: 6222*******8485
        /// </code>
        /// </summary>
        /// <param name="value">需要替换的字符串</param>
        /// <param name="startLen">前保留长度</param>
        /// <param name="endLen">尾保留长度</param>
        /// <param name="replaceChar">特殊字符</param>
        /// <returns>被特殊字符替换的字符串</returns>
        public static string ReplaceWithSpecialChar(string value, int startLen = 4, int endLen = 4, char specialChar = '*')
        {
            try
            {
                int lenth = value.Length - startLen - endLen;
                string replaceStr = value.Substring(startLen, lenth);
                string specialStr = string.Empty;
                for (int i = 0; i < replaceStr.Length; i++)
                {
                    specialStr += specialChar;
                }
                value = value.Replace(replaceStr, specialStr);
            }
            catch (Exception)
            {
                //throw;
            }
            return value;
        }
        /// <summary>
        /// 提取字符串中的汉字
        /// </summary>
        public static string GetChineseWord(string oriText)
        {
            try
            {
                string x = @"[\u4E00-\u9FFF]+";
                MatchCollection Matches = Regex.Matches(oriText, x, RegexOptions.IgnoreCase);
                StringBuilder sb = new StringBuilder();
                foreach (Match NextMatch in Matches)
                {
                    sb.Append(NextMatch.Value);
                }
                return sb.ToString();
            }
            catch
            {
                return "";
            }
        }
    }
}
