﻿/*******************************************************************************
 * Copyright © 2016 NFine.Framework 版权所有
 * Author: NFine
 * Description: NFine快速开发平台
 * Website：http://www.nfine.cn
*********************************************************************************/
using System;

namespace System
{
    /// <summary>
    /// 创建用户
    /// </summary>
    public interface ICreationAudited
    {
        string AddUserID { get; set; }
    }
 
}