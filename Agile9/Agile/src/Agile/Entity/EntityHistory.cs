﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace System
{
    /// <summary>
    /// 实体更改历史记录
    /// </summary>
    [Table("EntityHistory")]
    [Description("实体更改历史记录")]
    public class EntityHistory : Entity
    {
        /// <summary>
        /// 表的名称
        /// </summary>
        [Description("表名")]
        public string TableName { get; set; }
        /// <summary>
        /// 源行ID
        /// </summary>
        [Description("源行ID")]
        public string RecordID { get; set; }
        /// <summary>
        /// 更改类型 EntityState
        /// </summary>
        [Description("更改类型")]
        public int State { get; set; }
        /// <summary>
        /// 更改字段
        /// </summary>
        [Description("更改字段")]
        public string ColumnName { get; set; }
        /// <summary>
        /// 源值
        /// </summary>
        [Description("源值")]
        public string OriginalValue { get; set; }
        /// <summary>
        /// 新值
        /// </summary>
        [Description("新值")]
        public string NewValue { get; set; }
        /// <summary>
        /// 操作人
        /// </summary>
        [Description("操作人")]
        public string UserID { get; set; }
    }
}
