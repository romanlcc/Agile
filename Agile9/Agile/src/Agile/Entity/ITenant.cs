﻿namespace System
{
    /// <summary>
    /// 为必须具有TenantId的实体实现此接口。
    /// </summary>
    public interface ITenant
    {
        string TenantId { get; set; }
    }
}
