﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace System
{
    public static class EntityHelper
    {
        public static bool IsEntity(Type type)
        {
            return AttributeHelper.IsAssignableToGenericType(type, typeof(IEntity<>));
        }
        public static bool IsTreeEntity(Type type)
        {
            return AttributeHelper.IsAssignableToGenericType(type, typeof(TreeEntity<>));
        }
        /// <summary>
        /// 获取所有实体类
        /// </summary>
        public static IEnumerable<Type> GetEntityTypes(Assembly assembly)
        {
            var types = assembly.GetExportedTypes().Where(t => EntityHelper.IsEntity(t));
            return types;
        }
    }
}
