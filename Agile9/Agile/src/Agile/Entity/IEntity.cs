﻿namespace System
{
    public interface IEntity : IEntity<string>
    {

    }
    public interface IEntity<TPrimaryKey>
    {
        TPrimaryKey ID { get; set; }
    }
}
