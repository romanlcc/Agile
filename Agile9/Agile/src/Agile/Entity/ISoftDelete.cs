﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace System
{
    /// <summary>
    /// 软删除
    /// </summary>
    public interface ISoftDelete
    {
        string DeleteUserID { get; set; }
        /// <summary>
        /// 软删除 0正常1删除
        /// </summary>
        [Description("软删除")]
        [Display(Name = "软删除")]
        int IsDelete { get; set; }

        DateTime? DeleteTime { get; set; }
        
    }

    public enum EntityChangeType : int
    {
        Created = 0,
        Updated = 1,
        Deleted = 2
    }
}
