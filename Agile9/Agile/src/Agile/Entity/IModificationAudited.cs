﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System
{
    /// <summary>
    /// 修改用户
    /// </summary>
    public interface IModificationAudited
    {
        string EditUserID { get; set; }
        DateTime? EditTime { get; set; }
    }
}
