﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agile
{
	/// <summary>
	///使用许可证Agile指定许可证类型下必须指定才能使用库
	/// </summary>
	public enum LicenseContext
	{
		/// <summary>
		/// 您遵守非商业许可证 https://polyformproject.org/licenses/noncommercial/1.0.0/
		/// </summary>
		NonCommercial,
		/// <summary>
		/// 您遵守商业许可证 https://epplussoftware.com/licenseoverview
		/// </summary>
		Commercial = 0
	}
}
