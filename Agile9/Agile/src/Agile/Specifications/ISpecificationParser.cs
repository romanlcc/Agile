﻿namespace System.Linq {
    public interface ISpecificationParser<TCriteria>
    {
        TCriteria Parse<T>(ISpecification<T> specification);
    }
}
