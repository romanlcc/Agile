﻿using System;
using System.Data;
using System.Diagnostics;

namespace System
{
    /// <summary>
    /// 一个简单的日志记录类。
    /// 
    /// 目前只有处理异常的方法。
    /// </summary>
    public static class Logger
    {
        private static object _lock = new object();
        private static LoggerBase _impl = new FileLogger();

        /// <summary>
        /// 使用具体的日志记录器来接管本 API。
        /// </summary>
        /// <param name="loggerImpl"></param>
        public static void SetImplementation(LoggerBase loggerImpl)
        {
            _impl = loggerImpl;
        }
        public static void LogInfo(string message)
        {
            lock (_lock)
            {
                try
                {
                    Debug.WriteLine(message);
                    _impl.LogInfo(message);
                }
                catch { }
            }
        }
        public static void LogWarn(string message)
        {
            lock (_lock)
            {
                try
                {
                    Debug.WriteLine(message);
                    _impl.LogWarn(message);
                }
                catch { }
            }
        }
        public static void LogDebug(string message)
        {
            lock (_lock)
            {
                try
                {
                    Debug.WriteLine(message);
                    _impl.LogDebug(message);
                }
                catch { }
            }
        }
        public static void LogTrace(string message)
        {
            lock (_lock)
            {
                try
                {
                    Debug.WriteLine(message);
                    _impl.LogTrace(message);
                }
                catch { }
            }
        }
        /// <summary>
        /// 记录某个已经生成的异常到文件中。
        /// </summary>
        /// <param name="title"></param>
        /// <param name="e"></param>
        public static void LogError(string title, Exception e)
        {
            lock (_lock)
            {
                try
                {
                    Debug.WriteLine(title+e.Message);
                    _impl.LogError(title, e);
                }
                catch { }
            }
        }
        /// <summary>
        /// 记录某个已经生成的异常到文件中。
        /// </summary>
        /// <param name="title"></param>
        /// <param name="e"></param>
        public static void LogError(string message)
        {
            lock (_lock)
            {
                try
                {
                    Debug.WriteLine(message);
                    _impl.LogError(message);
                }
                catch { }
            }
        }
        public static void LogSql(string message)
        {
            lock (_lock)
            {
                try
                {
                    Debug.WriteLine(message);
                    _impl.LogSql(message);
                }
                catch { }
            }
        }
    }
}