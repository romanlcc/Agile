﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace System
{
    /// <summary>
    /// 日志记录器。
    /// </summary>
    public abstract class LoggerBase
    {
        public virtual void LogInfo(string message) { }
        public virtual void LogWarn(string message) { }
        public virtual void LogDebug(string message) { }
        public virtual void LogTrace(string message) { }
        public virtual void LogSql(string message) { }
        public virtual void LogError(string title, Exception e) { }
        public virtual void LogError(string message) { }
    }
    public enum LogType
    {
        Info, 
        Warn,
        Debug, 
        Error,
        Trace,
        Sql
    }
}