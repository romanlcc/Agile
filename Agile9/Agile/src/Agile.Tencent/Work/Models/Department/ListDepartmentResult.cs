﻿using System.Collections.Generic;

namespace Agile.Tencent.Work
{
    /// <summary>
    /// 获取部门列表的返回结果类
    /// </summary>
    public class ListDepartmentResult : JsonResult
    {
        /// <summary>
        /// 部门列表数据。
        /// </summary>
        public List<ListDepartmentInfo> department;
    }
}
