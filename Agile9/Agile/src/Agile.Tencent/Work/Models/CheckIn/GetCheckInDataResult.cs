﻿using System.Collections.Generic;

namespace Agile.Tencent.Work
{
    /// <summary>
    /// 获取打卡数据的返回结果类
    /// </summary>
    public class GetCheckInDataResult : JsonResult
    {
        /// <summary>
        /// 打卡数据列表
        /// </summary>
        public List<GetCheckInDataInfo> checkindata { get; set; }
    }
}
