﻿using System.Collections.Generic;

namespace Agile.Tencent.Work
{
    /// <summary>
    /// 获取部门成员的返回结果类
    /// </summary>
    public class SimpleListUserResult : JsonResult
    {
        /// <summary>
        /// 成员列表
        /// </summary>
        public List<SimpleListUserInfo> userlist { get; set; }
    }
}
