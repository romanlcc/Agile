﻿using System.Collections.Generic;

namespace Agile.Tencent.Work
{
    /// <summary>
    /// 获取标签列表的返回结果类
    /// </summary>
    public class ListTagResult : JsonResult
    {
        /// <summary>
        /// 标签列表
        /// </summary>
        public List<TagInfo> taglist { get; set; }
    }
}
