using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace System.Data
{
    /// <summary>
    /// 库
    /// </summary>
    [DebuggerDisplay("{DatabaseName}")]
    [Serializable]
    public class DataBaseModel
    {
        public DataBaseModel()
        {
            Tables = new List<TableModel>();
        }
        public string DatabaseName { get; set; }
        public List<TableModel> Tables { get; set; }
    }
    /// <summary>
    /// 表
    /// </summary>
    [DebuggerDisplay("{TableName}")]
    [Serializable]
    public class TableModel
    {
        public TableModel()
        {
            Columns = new List<TableColumn>();
        }
        public string TableName { get; set; }
        /// <summary>
        /// 说明
        /// </summary>
        public string TableNameRemark { get; set; }
        /// <summary>
        /// 主键列
        /// </summary>
        public string TablePK { get; set; }
        public bool IsTree { get; set; }
        /// <summary>
        /// 使用大小
        /// </summary>
        public string Reserved { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Data { get; set; }
        /// <summary>
        /// 索引使用大小
        /// </summary>
        public string IndexSize { get; set; }
        /// <summary>
        /// 记录数
        /// </summary>
        public string SumRows { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Unused { get; set; }
        public List<TableColumn> Columns { get; set; }
        public override string ToString()
        {
            return "Name:" + TableName + " Remark:" + TableNameRemark;
        }
    }
    /// <summary>
    /// 字段
    /// </summary>
    [DebuggerDisplay("{ColumnName}")]
    [Serializable]
    public class TableColumn
    {
        public TableColumn()
        {
            IsPrimaryKey = false;
            IsIdentity = false;
            IsNullable = false;
            MaxLength = 50;
        }
        /// <summary>
        /// 所属表
        /// </summary>
        public string TableName { get; set; }
        /// <summary>
        /// 序号
        /// </summary>
        public int ColumnOrder { get; set; }
        /// <summary>
        /// 是否主键
        /// </summary>
        public bool IsPrimaryKey { get; set; }
        /// <summary>
        /// 标识  
        /// </summary>
        public bool IsIdentity { get; set; }
        /// <summary>
        /// 允许空 
        /// </summary>
        public bool IsNullable { get; set; }
        /// <summary>
        /// 字段大小
        /// </summary>
        public int MaxLength { get; set; }
        /// <summary>
        /// 字段名称
        /// </summary>
        public string ColumnName { get; set; }
        /// <summary>
        /// 字段类型
        /// </summary>
        public string ColumnType { get; set; }
        /// <summary>
        /// 字段说明
        /// </summary>
        public string ColumnRemark { get; set; }
        /// <summary>
        /// 默认值
        /// </summary>
        public string DefaultValue { get; set; }
        /// <summary>
        /// 字段说明[字段名称]
        /// </summary>
        public string ColumnNameRemark
        {
            get { return ColumnRemark + "[" + ColumnName + "]"; }
        }
        public override string ToString()
        {
            return "Name:" + ColumnName + " Remark:" + ColumnRemark;
        }
    }
    /// <summary>
    ///  表关系
    /// </summary>
    [Serializable]
    public class TableRelation
    {
        public string FTableName { get; set; }
        public string FColumnsName { get; set; }
        public string RTableName { get; set; }
        public string RColumnsName { get; set; }
        public bool CnstIsUpdateCascade { get; set; }
        public bool CnstIsDeleteCascade { get; set; }
    }
}
