﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System.Data
{
    /// <summary>
    /// 数据库配置
    /// </summary>
    public class DbOptions
    {
        /// <summary>
        /// 是否开启日志
        /// </summary>
        public bool Logging { get; set; }
        /// <summary>
        /// 数据库类型
        /// </summary>
        public DatabaseType DbType { get; set; }
        /// <summary>
        /// 获取连接字符串
        /// </summary>
        public string ConnectionString { get; set; }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("DbType:" + DbType).Append(" ConnectionString:" + ConnectionString);
            return stringBuilder.ToString();
        }
    }
}
