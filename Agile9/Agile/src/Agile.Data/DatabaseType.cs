﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace System.Data
{
    /// <summary>
    /// 数据库类型定义
    /// </summary>
    public enum DatabaseType
    {
        //SQLServer数据库
        [Description("SqlServer")]
        SqlServer = 0,
        //Mysql数据库
        [Description("MySql")]
        MySql = 1,
        //Oracle数据库
        [Description("Oracle")]
        Oracle = 2,
        //SQLite数据库
        [Description("Sqlite")]
        Sqlite = 3,
    }
}
