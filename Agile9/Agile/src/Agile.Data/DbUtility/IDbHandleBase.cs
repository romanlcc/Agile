﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using DapperExtensions;

namespace System.Data
{
    public interface IDbHandleBase
    {
        bool TestDataGenerate(string tableName, int count = 20);
        bool AddTableFieldInfo(string tableName, string fieldName, string info);
        string CheckSql(string strSql);
        bool CleanAllTableData<TEntity>() where TEntity : class;
        IDbConnection CreateConnectionAndOpen();
        bool CreateTable(Type type);
        bool CreateTable<TEntity>(bool cover = true) where TEntity : class;
        string GetDbTableName(Type type);
        string GetDbTableName<TEntity>() where TEntity : class;
        bool Delete<TEntity>(object predicate, IDbTransaction transaction = null, int? cmdTimeout = 60) where TEntity : class;
        bool Delete<TEntity>(TEntity entity, IDbTransaction transaction = null, int? cmdTimeout = 60) where TEntity : class;
        bool DropTable<TEntity>() where TEntity : class;
        List<DataBaseModel> GetDataBases();
        List<TableModel> GetTables();
        List<TableColumn> GetTableColumns(string tableName = "");
        DataSet ExecuteDataSet(string sql, dynamic param = null, int? cmdTimeout = 60, IDbTransaction tran = null, CommandType? cmdType = null);
        DataTable ExecuteDataTable(string sql, dynamic param = null, int? cmdTimeout = 60, IDbTransaction tran = null, CommandType? cmdType = null);
        int ExecuteNonQuery(string sql, dynamic param = null, int? cmdTimeout = 60, IDbTransaction tran = null, CommandType? cmdType = null);
        object ExecuteScalar(string sql, dynamic param = null, int? cmdTimeout = 60, IDbTransaction tran = null, CommandType? cmdType = null);
        TEntity ExecuteScalar<TEntity>(string sql, dynamic param = null, int? cmdTimeout = 60, IDbTransaction tran = null, CommandType? cmdType = null);
        int ExecuteSqlTran(List<string> SQLStringList);
        IEnumerable<T> GetPageSql<T>(string files, string tableName, string where, string orderby, int pageIndex, int pageSize, out int total);
        string GetTopSql(string selectSql, int top);
        void Insert<TEntity>(IEnumerable<TEntity> entities, IDbTransaction transaction = null, int? cmdTimeout = 60) where TEntity : class;
        dynamic Insert<TEntity>(TEntity entity, IDbTransaction transaction = null, int? cmdTimeout = 60) where TEntity : class;
        bool Update<TEntity>(TEntity entity, IDbTransaction transaction = null, int? cmdTimeout = 60) where TEntity : class;
        TEntity Get<TEntity>(int id, int? cmdTimeout = 60) where TEntity : class;
        TEntity Get<TEntity>(string id, int? cmdTimeout = 60) where TEntity : class;
        List<TEntity> GetList<TEntity>(Expression<Func<TEntity, bool>> predicate = null) where TEntity : class;
        bool Exist<TEntity>(Expression<Func<TEntity, bool>> predicate = null) where TEntity : class;
        TEntity GetFirstOrDefault<TEntity>(Expression<Func<TEntity, bool>> predicate = null)where TEntity : class;
        IEnumerable<TEntity> Query<TEntity>(string sql, dynamic param = null, int? cmdTimeout = 60, bool buffered = true, CommandType? cmdType = null) where TEntity : class;
        IEnumerable<dynamic> QueryDynamics(string sql, dynamic param = null, int? cmdTimeout = 60, bool buffered = true, CommandType? cmdType = null);
        List<Hashtable> QueryHashtables(string sql, dynamic param = null, int? cmdTimeout = 60, bool buffered = false, CommandType? cmdType = null);
        List<List<dynamic>> QueryMultipleDynamic(string sql, dynamic param = null, int? cmdTimeout = 60, IDbTransaction tran = null, CommandType? cmdType = null);
        List<List<Hashtable>> QueryMultipleHashtables(string sql, dynamic param = null, int? cmdTimeout = 60, IDbTransaction tran = null, CommandType? cmdType = null);
        IEnumerable<TEntity> GetList<TEntity>(object predicate = null, IList<ISort> sort = null, int? cmdTimeout = 60, bool buffered = true) where TEntity : class;
        IEnumerable<TEntity> GetPageByModel<TEntity>(object predicate, IList<ISort> sort, int startPageIndex, int resultsPerPage, IDbTransaction transaction = null, int? commandTimeout = null, bool buffered = true) where TEntity : class;
    }
}