﻿using DapperExtensions.Mapper;
using System;
using System.Text;
using System.Collections.Generic;
using System.Reflection;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.SQLite;
using System.Data.SqlClient;
using System.Data.OracleClient;
using Dapper;
using DapperExtensions;
using DapperExtensions.Sql;
using MySql.Data.MySqlClient;

namespace System.Data
{
    public class SqliteHandle : DbHandleBase
    {
        #region 初始化必须
        /// <summary>
        ///初始化，需要传入连接字符串
        /// </summary>
        /// <param name="conStr">连接字符串</param>
        /// <returns></returns>
        public SqliteHandle(string conStr) : base(conStr)
        {
            //DapperExtensions默认是用SqlServer的标记，这边使用Sqlite的特殊语法标记，需要在初始化时给予指定。
            //DapperExtensions是静态的，全局只能支持一种数据库 
            DapperExtensions.DapperExtensions.SqlDialect = new DapperExtensions.Sql.SqliteDialect();
            DbType = DatabaseType.Sqlite;
        }
        /// <summary>
        /// 创建连接对象。子类需要重写，根据自己的数据库类型实例化自己的数据库连接对象(支持IDbConnection接口)。
        /// Dapper可以在所有Ado.net Providers下工作，包括sqlite, sqlce, firebird, oracle, MySQL, PostgreSQL and SQL Server。
        /// </summary>
        /// <returns></returns>
        protected override IDbConnection CreateConnection()
        {
            IDbConnection connection = new SQLiteConnection(ConnectionString);// System.Data.SQLite
            return connection;
        }
        #endregion

        #region 数据结构
        public override bool AddTableFieldInfo(string tableName, string fieldName, string info)
        {
            throw new NotImplementedException();
        }

        public override bool CleanAllTableData<T>()
        {
            throw new NotImplementedException();
        }

        public override bool CreateTable(Type type)
        {
            throw new NotImplementedException();
        }

        public override bool CreateTable<T>(bool cover = true)
        {
            throw new NotImplementedException();
        }

        public override bool DropTable<T>()
        {
            throw new NotImplementedException();
        }

        public override List<DataBaseModel> GetDataBases()
        {
            throw new NotImplementedException();
        }

        public override List<TableColumn> GetTableColumns(string tableName = "")
        {
            throw new NotImplementedException();
        }

        public override List<TableModel> GetTables()
        {
            throw new NotImplementedException();
        }
        public override bool TestDataGenerate(string tableName, int count = 20)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
