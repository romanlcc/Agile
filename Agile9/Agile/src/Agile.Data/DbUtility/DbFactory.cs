﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System.Data
{
    public class DbFactory
    {
        public static IDbHandleBase DbHelper(string strConn = "", DatabaseType databaseType = DatabaseType.SqlServer, string environmentName = "")
        {
            IDbHandleBase dbHandleBase = null;
            if (string.IsNullOrEmpty(strConn))
            {
                #region 读取配置文件
                var dbOptions = new ConfigurationHelper().Get<DbOptions>("Db", environmentName);
                switch (dbOptions.DbType)
                {
                    case DatabaseType.SqlServer:
                        dbHandleBase = new SqlServerHandle(dbOptions.ConnectionString);
                        break;
                    case DatabaseType.MySql:
                        dbHandleBase = new MySqlHandle(dbOptions.ConnectionString);
                        break;
                    case DatabaseType.Oracle:
                        dbHandleBase = new OracleHandle(dbOptions.ConnectionString);
                        break;
                    case DatabaseType.Sqlite:
                        dbHandleBase = new SqliteHandle(dbOptions.ConnectionString);
                        break;
                    default:
                        break;
                }
                #endregion
            }
            else
            {
                #region 动态配置文件
                switch (databaseType)
                {
                    case DatabaseType.SqlServer:
                        dbHandleBase = new SqlServerHandle(strConn);
                        break;
                    case DatabaseType.MySql:
                        dbHandleBase = new MySqlHandle(strConn);
                        break;
                    case DatabaseType.Oracle:
                        dbHandleBase = new OracleHandle(strConn);
                        break;
                    case DatabaseType.Sqlite:
                        dbHandleBase = new SqliteHandle(strConn);
                        break;
                    default:
                        break;
                }
                #endregion
            }
            if(dbHandleBase==null) throw new Exception("数据库配置错误");
            return dbHandleBase;
        }
    }
}
