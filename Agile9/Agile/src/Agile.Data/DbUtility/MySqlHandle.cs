﻿using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Text;
using Dapper;
using DapperExtensions;
using System.Reflection;
using DapperExtensions.Sql;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Data.OracleClient;

namespace System.Data
{
    public class MySqlHandle : DbHandleBase
    {
        #region 初始化必须
        /// <summary>
        ///初始化，需要传入连接字符串
        /// </summary>
        /// <param name="conStr">连接字符串</param>
        /// <returns></returns>
        public MySqlHandle(string conStr) : base(conStr)
        {
            //DapperExtensions默认是用SqlServer的标记，这边使用MySql的特殊语法标记，需要在初始化时给予指定。
            //DapperExtensions是静态的，全局只能支持一种数据库 
            DapperExtensions.DapperExtensions.SqlDialect = new DapperExtensions.Sql.MySqlDialect();
            DbType = DatabaseType.MySql;
        }

        /// <summary>
        /// 创建连接对象。子类需要重写，根据自己的数据库类型实例化自己的数据库连接对象(支持IDbConnection接口)。
        /// Dapper可以在所有Ado.net Providers下工作，包括sqlite, sqlce, firebird, oracle, MySQL, PostgreSQL and SQL Server。
        /// </summary>
        /// <returns></returns>
        protected override IDbConnection CreateConnection()
        {
            IDbConnection connection = new MySqlConnection(ConnectionString);// MySql.Data.MySqlClient
            return connection;
        }
        #endregion

        #region 数据结构
        public override bool AddTableFieldInfo(string tableName, string fieldName, string info)
        {
            throw new NotImplementedException();
        }

        public override bool CleanAllTableData<T>()
        {
            throw new NotImplementedException();
        }

        public override bool CreateTable(Type type)
        {
            throw new NotImplementedException();
        }

        public override bool CreateTable<T>(bool cover = true)
        {
            throw new NotImplementedException();
        }

        public override bool DropTable<T>()
        {
            throw new NotImplementedException();
        }
        public override List<DataBaseModel> GetDataBases()
        {
            throw new NotImplementedException();
        }

        public override List<TableColumn> GetTableColumns(string tableName = "")
        {
            string sql = string.Format("select column_name,data_type,column_key,Extra,character_maximum_length " +
                "from information_schema.COLUMNS where table_name = '{1}';  ", tableName);

            var list = new List<TableColumn>();
            var dt = ExecuteDataTable(sql);
            foreach (DataRow row in dt.Rows)
            {
                var tc = new TableColumn();
                tc.TableName = tableName;
                tc.ColumnName = row["column_name"].ToString();
                tc.ColumnRemark = row["Extra"].ToString();
                tc.ColumnType = row["data_type"].ToString();
                tc.DefaultValue ="";
                tc.MaxLength = int.Parse(row["character_maximum_length"].ToString());
                if (row["column_key"].ToString() == "√")
                {
                    tc.IsPrimaryKey = true;
                }
                list.Add(tc);
            }
            return list;
        }

        public override List<TableModel> GetTables()
        {
            string sql = "select table_name from information_schema.tables where table_type='base table'";
            var list = new List<TableModel>();
            var dt = ExecuteDataTable(sql);
            foreach (DataRow row in dt.Rows)
            {
                string tableName = row["table_name"].ToString();
                var tm = new TableModel();
                tm.TableName = tableName;
                tm.TableNameRemark ="";
                if (tm.Columns == null || tm.Columns.Count == 0) tm.Columns = GetTableColumns(tm.TableName);
                if (tm.Columns.Find(p => p.ColumnName == "ParentID") != null)
                {
                    tm.IsTree = true;
                }
                var mo = tm.Columns.FindLast(p => p.IsPrimaryKey == true);
                if (mo != null)
                {
                    tm.TablePK = mo.ColumnName;
                }
                list.Add(tm);
            }
            return list;
        }
        public override bool TestDataGenerate(string tableName, int count = 20)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
