﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace System.Data
{
    public interface IRepository<TEntity> where TEntity : Entity
    {
        string Add(TEntity model);
        TEntity AddOrUpdate(TEntity model);
        void BatchAdd(IEnumerable<TEntity> models);
        bool Delete(string Idlist);
        bool Delete(Expression<Func<TEntity, bool>> predicate = null);
        bool Delete(params TEntity[] models);
        bool DeleteWhere(string sqlWhere);
        bool Update(TEntity model);
        bool UpdateField(string keyId, string fieldName, string fieldValue);
        bool Exists(string Id);
        bool Exist(Expression<Func<TEntity, bool>> predicate = null);
        List<TEntity> Get(Expression<Func<TEntity, bool>> predicate = null);
        List<TEntity> GetByAll(string strWhere = "");
        int GetTotalCount(string strWhere = "");
        TEntity GetById(string Id);
        List<TEntity> GetByPage(string strWhere, string orderby = "OrderBy desc", int startIndex = 1, int endIndex = 10);
        //string GetGridData(string strWhere, string field="*", string sidx= "OrderBy", string sord= "desc", int page = 1, int rows = 15);

        JQridPageData GetJGridPageData(Paging paging);


    }
}