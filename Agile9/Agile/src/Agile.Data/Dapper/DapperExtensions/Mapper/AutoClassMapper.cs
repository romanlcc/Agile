using System.Text;
using System.Linq;
using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DapperExtensions.Mapper
{
    /// <summary>
    /// Automatically maps an entity to a table using a combination of reflection and naming conventions for keys.
    /// </summary>
    public class AutoClassMapper<T> : ClassMapper<T> where T : class
    {
        public AutoClassMapper()
        {
            Type type = typeof(T);
            //TODO:���ñ���
            var tableCfgInfo = AttributeHelper.Get<T, TableAttribute>();
            var tableName= tableCfgInfo != null ? tableCfgInfo.Name.Trim() : typeof(T).Name;

            Table(tableName);
            AutoMap();
        }
    }
}