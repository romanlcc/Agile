using System;
using Agile;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace System.Data
{
    public class DbStartup : IAgileStartup
    {
        public int Order => 7000;

        public void Configure(IApplicationBuilder app)
        {

        }

        public void ConfigureMvc(MvcOptions mvcOptions)
        {

        }

        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped(typeof(IRepository<>), typeof(DapperRepository<>));
        }
    }
}
