﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Agile.NetWork
{
    /// <summary>
    /// 所有数据转换类的静态辅助方法
    /// Static helper method for all data conversion classes
    /// </summary>
    public static class ByteTransformHelper
    {

        /// <summary>
        /// 结果转换操作的基础方法，需要支持类型，及转换的委托
        /// </summary>
        /// <typeparam name="TResult">结果类型</typeparam>
        /// <param name="result">源</param>
        /// <param name="translator">实际转换的委托</param>
        /// <returns>转换结果</returns>
        public static Result<TResult> GetResultFromBytes<TResult>( Result<byte[]> result, Func<byte[], TResult> translator )
        {
            try
            {
                if (result.Success)
                {
                    return Result.CreateSuccessResult(translator( result.Data ));
                }
                else
                {
                    return Result.CreateFailedResult<TResult>( result );
                }
            }
            catch (Exception ex)
            {
                return new Result<TResult>() { Message = "数据转换失败，源数据：" + Utils.ByteToHexString(result.Data) + " : Length({result.Data.Length}) " + ex.Message };
            }
        }

        /// <summary>
        /// 结果转换操作的基础方法，需要支持类型，及转换的委托
        /// </summary>
        /// <typeparam name="TResult">结果类型</typeparam>
        /// <param name="result">源结果</param>
        /// <returns>转换结果</returns>
        public static Result<TResult> GetResultFromArray<TResult>( Result<TResult[]> result )
        {
            if (!result.Success) return Result.CreateFailedResult<TResult>( result );

            return Result.CreateSuccessResult( result.Data[0] );
        }
        
    }
}
