﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Agile.NetWork.Serial;

namespace Agile.NetWork.ModBus
{
    /// <summary>
    /// Modbus-Rtu通讯协议的类库，多项式码0xA001
    /// </summary>
    /// <remarks>
    /// 本客户端支持的标准的modbus-tcp协议，内置的消息号会进行自增，地址格式采用富文本表示形式
    /// <note type="important">
    /// 地址共可以携带3个信息，最完整的表示方式"s=2;x=3;100"，对应的modbus报文是 02 03 00 64 00 01 的前四个字节，站号，功能码，起始地址，下面举例
    /// <list type="definition">
    /// <item>
    /// <term>读取线圈</term>
    /// <description>ReadCoil("100")表示读取线圈100的值，ReadCoil("s=2;100")表示读取站号为2，线圈地址为100的值</description>
    /// </item>
    /// <item>
    /// <term>读取离散输入</term>
    /// <description>ReadDiscrete("100")表示读取离散输入100的值，ReadDiscrete("s=2;100")表示读取站号为2，离散地址为100的值</description>
    /// </item>
    /// <item>
    /// <term>读取寄存器</term>
    /// <description>ReadInt16("100")表示读取寄存器100的值，ReadInt16("s=2;100")表示读取站号为2，寄存器100的值</description>
    /// </item>
    /// <item>
    /// <term>读取输入寄存器</term>
    /// <description>ReadInt16("x=4;100")表示读取输入寄存器100的值，ReadInt16("s=2;x=4;100")表示读取站号为2，输入寄存器100的值</description>
    /// </item>
    /// </list>
    /// 对于写入来说也是一致的
    /// <list type="definition">
    /// <item>
    /// <term>写入线圈</term>
    /// <description>WriteCoil("100",true)表示读取线圈100的值，WriteCoil("s=2;100",true)表示读取站号为2，线圈地址为100的值</description>
    /// </item>
    /// <item>
    /// <term>写入寄存器</term>
    /// <description>Write("100",(short)123)表示写寄存器100的值123，Write("s=2;100",(short)123)表示写入站号为2，寄存器100的值123</description>
    /// </item>
    /// </list>
    /// </note>
    /// </remarks>
    public class ModbusRtu : SerialDeviceBase<ReverseWordTransform>
    {
        #region Constructor

        /// <summary>
        /// 实例化一个Modbus-Rtu协议的客户端对象
        /// </summary>
        public ModbusRtu( )
        {
            ByteTransform = new ReverseWordTransform( );
        }


        /// <summary>
        /// 指定服务器地址，端口号，客户端自己的站号来初始化
        /// </summary>
        /// <param name="station">客户端自身的站号</param>
        public ModbusRtu( byte station = 0x01 )
        {
            ByteTransform = new ReverseWordTransform( );
            this.station = station;
        }

        #endregion

        #region Private Member

        private byte station = ModbusInfo.ReadCoil;                  // 本客户端的站号
        private bool isAddressStartWithZero = true;                  // 线圈值的地址值是否从零开始

        #endregion

        #region Public Member
        
        /// <summary>
        /// 获取或设置起始的地址是否从0开始，默认为True
        /// </summary>
        /// <remarks>
        /// <note type="warning">因为有些设备的起始地址是从1开始的，就要设置本属性为<c>True</c></note>
        /// </remarks>
        public bool AddressStartWithZero
        {
            get { return isAddressStartWithZero; }
            set { isAddressStartWithZero = value; }
        }

        /// <summary>
        /// 获取或者重新修改服务器的默认站号信息
        /// </summary>
        /// <remarks>
        /// 当你调用 ReadCoil("100") 时，对应的站号就是本属性的值，当你调用 ReadCoil("s=2;100") 时，就忽略本属性的值，读写寄存器的时候同理
        /// </remarks>
        public byte Station
        {
            get { return station; }
            set { station = value; }
        }
        
        /// <summary>
        /// 获取或设置数据解析的格式，默认ABCD，可选BADC，CDAB，DCBA格式
        /// </summary>
        /// <remarks>
        /// 对于Int32,UInt32,float,double,Int64,UInt64类型来说，存在多地址的电脑情况，需要和服务器进行匹配
        /// </remarks>
        public DataFormat DataFormat
        {
            get { return ByteTransform.DataFormat; }
            set { ByteTransform.DataFormat = value; }
        }

        /// <summary>
        /// 字符串数据是否按照字来反转
        /// </summary>
        /// <remarks>
        /// 字符串按照2个字节的排列进行颠倒，根据实际情况进行设置
        /// </remarks>
        public bool IsStringReverse
        {
            get { return ByteTransform.IsStringReverse; }
            set { ByteTransform.IsStringReverse = value; }
        }
        
        #endregion

        #region Build Command
        
        /// <summary>
        /// 生成一个读取线圈的指令头
        /// </summary>
        /// <param name="address">地址</param>
        /// <param name="count">长度</param>
        /// <returns>携带有命令字节</returns>
        public Result<byte[]> BuildReadCoilCommand( string address, ushort count )
        {
            Result<ModbusAddress> analysis = ModbusInfo.AnalysisReadAddress( address, isAddressStartWithZero );
            if (!analysis.Success) return Result.CreateFailedResult<byte[]>( analysis );
            
            // 生成最终tcp指令
            byte[] buffer = ModbusInfo.PackCommandToRtu( analysis.Data.CreateReadCoils( station, count ) );
            return Result.CreateSuccessResult( buffer );
        }

        /// <summary>
        /// 生成一个读取离散信息的指令头
        /// </summary>
        /// <param name="address">地址</param>
        /// <param name="length">长度</param>
        /// <returns>携带有命令字节</returns>
        public Result<byte[]> BuildReadDiscreteCommand( string address, ushort length )
        {
            Result<ModbusAddress> analysis = ModbusInfo.AnalysisReadAddress( address, isAddressStartWithZero );
            if (!analysis.Success) return Result.CreateFailedResult<byte[]>( analysis );
            
            // 生成最终tcp指令
            byte[] buffer = ModbusInfo.PackCommandToRtu( analysis.Data.CreateReadDiscrete( station, length ) );
            return Result.CreateSuccessResult( buffer );
        }
        
        /// <summary>
        /// 生成一个读取寄存器的指令头
        /// </summary>
        /// <param name="address">地址</param>
        /// <param name="length">长度</param>
        /// <returns>携带有命令字节</returns>
        public Result<byte[]> BuildReadRegisterCommand( string address, ushort length )
        {
            Result<ModbusAddress> analysis = ModbusInfo.AnalysisReadAddress( address, isAddressStartWithZero );
            if (!analysis.Success) return Result.CreateFailedResult<byte[]>( analysis );
            
            // 生成最终rtu指令
            byte[] buffer = ModbusInfo.PackCommandToRtu( analysis.Data.CreateReadRegister( station, length ) );
            return Result.CreateSuccessResult( buffer );
        }
        
        /// <summary>
        /// 生成一个读取寄存器的指令头
        /// </summary>
        /// <param name="address">地址</param>
        /// <param name="length">长度</param>
        /// <returns>携带有命令字节</returns>
        private Result<byte[]> BuildReadRegisterCommand( ModbusAddress address, ushort length )
        {
            // 生成最终rtu指令
            byte[] buffer = ModbusInfo.PackCommandToRtu( address.CreateReadRegister( station, length ) );
            return Result.CreateSuccessResult( buffer );
        }
        
        /// <summary>
        /// 生成一个写入单线圈的指令头
        /// </summary>
        /// <param name="address">地址</param>
        /// <param name="value">长度</param>
        /// <returns>包含结果对象的报文</returns>
        public Result<byte[]> BuildWriteOneCoilCommand( string address, bool value )
        {
            Result<ModbusAddress> analysis = ModbusInfo.AnalysisReadAddress( address, isAddressStartWithZero );
            if (!analysis.Success) return Result.CreateFailedResult<byte[]>( analysis );
            
            // 生成最终rtu指令
            byte[] buffer = ModbusInfo.PackCommandToRtu( analysis.Data.CreateWriteOneCoil( station, value ) );
            return Result.CreateSuccessResult( buffer );
        }
        
        /// <summary>
        /// 生成一个写入单个寄存器的报文
        /// </summary>
        /// <param name="address">地址</param>
        /// <param name="data">长度</param>
        /// <returns>包含结果对象的报文</returns>
        public Result<byte[]> BuildWriteOneRegisterCommand( string address, byte[] data )
        {
            Result<ModbusAddress> analysis = ModbusInfo.AnalysisReadAddress( address, isAddressStartWithZero );
            if (!analysis.Success) return Result.CreateFailedResult<byte[]>( analysis );
            
            // 生成最终rtu指令
            byte[] buffer = ModbusInfo.PackCommandToRtu( analysis.Data.CreateWriteOneRegister( station, data ) );
            return Result.CreateSuccessResult( buffer );
        }
        
        /// <summary>
        /// 生成批量写入单个线圈的报文信息
        /// </summary>
        /// <param name="address">地址</param>
        /// <param name="values">实际数据值</param>
        /// <returns>包含结果对象的报文</returns>
        public Result<byte[]> BuildWriteCoilCommand( string address, bool[] values )
        {
            Result<ModbusAddress> analysis = ModbusInfo.AnalysisReadAddress( address, isAddressStartWithZero );
            if (!analysis.Success) return Result.CreateFailedResult<byte[]>( analysis );
            
            // 生成最终rtu指令
            byte[] buffer = ModbusInfo.PackCommandToRtu( analysis.Data.CreateWriteCoil( station, values ) );
            return Result.CreateSuccessResult( buffer );
        }
        
        /// <summary>
        /// 生成批量写入寄存器的报文信息
        /// </summary>
        /// <param name="address">地址</param>
        /// <param name="values">实际值</param>
        /// <returns>包含结果对象的报文</returns>
        public Result<byte[]> BuildWriteRegisterCommand( string address, byte[] values )
        {
            Result<ModbusAddress> analysis = ModbusInfo.AnalysisReadAddress( address, isAddressStartWithZero );
            if (!analysis.Success) return Result.CreateFailedResult<byte[]>( analysis );
            
            // 生成最终rtu指令
            byte[] buffer = ModbusInfo.PackCommandToRtu( analysis.Data.CreateWriteRegister( station, values ) );
            return Result.CreateSuccessResult( buffer );
        }
        
        #endregion
        
        #region Core Interative
        
        /// <summary>
        /// 检查当前的Modbus-Rtu响应是否是正确的
        /// </summary>
        /// <param name="send">发送的数据信息</param>
        /// <returns>带是否成功的结果数据</returns>
        protected virtual Result<byte[]> CheckModbusTcpResponse( byte[] send )
        {
            // 核心交互
            Result<byte[]> result = ReadBase( send );
            if (!result.Success) return result;

            // 长度校验
            if (result.Data.Length < 5) return new Result<byte[]>("接收的数据长度太短：" + "5");

            // 检查crc
            if (!SoftCRC16.CheckCRC16( result.Data )) return new Result<byte[]>( "Modbus的CRC校验检查失败");

            // 发生了错误
            if ((send[1] + 0x80) == result.Data[1]) return new Result<byte[]>( result.Data[2], ModbusInfo.GetDescriptionByErrorCode( result.Data[2] ) );

            if (send[1] != result.Data[1]) return new Result<byte[]>( result.Data[1], "Receive Command Check Failed: " );

            // 移除CRC校验
            byte[] buffer = new byte[result.Data.Length - 2];
            Array.Copy( result.Data, 0, buffer, 0, buffer.Length );
            return Result.CreateSuccessResult( buffer );
            
        }

        #endregion

        #region Protect Override

        /// <summary>
        /// 检查当前接收的字节数据是否正确的
        /// </summary>
        /// <param name="rBytes">从设备反馈回来的数据</param>
        /// <returns>是否校验成功</returns>
        protected override bool CheckReceiveBytes( byte[] rBytes )
        {
            return SoftCRC16.CheckCRC16( rBytes );
        }

        #endregion

        #region Read Support

        /// <summary>
        /// 读取服务器的数据，需要指定不同的功能码
        /// </summary>
        /// <param name="code">指令</param>
        /// <param name="address">地址</param>
        /// <param name="length">长度</param>
        /// <returns>带结果信息的字节返回数据</returns>
        protected Result<byte[]> ReadModBusBase( byte code, string address, ushort length )
        {
            Result<byte[]> command = null;
            switch (code)
            {
                case ModbusInfo.ReadCoil:
                    {
                        command = BuildReadCoilCommand( address, length );
                        break;
                    }
                case ModbusInfo.ReadDiscrete:
                    {
                        command = BuildReadDiscreteCommand( address, length );
                        break;
                    }
                case ModbusInfo.ReadRegister:
                    {
                        command = BuildReadRegisterCommand( address, length );
                        break;
                    }
                default: command = new Result<byte[]>() { Message = "不支持的功能码" }; break;
            }
            if (!command.Success) return Result.CreateFailedResult<byte[]>( command );

            Result<byte[]> resultBytes = CheckModbusTcpResponse( command.Data );
            if (resultBytes.Success)
            {
                // 二次数据处理
                if (resultBytes.Data.Length >= 3)
                {
                    byte[] buffer = new byte[resultBytes.Data.Length - 3];
                    Array.Copy( resultBytes.Data, 3, buffer, 0, buffer.Length );
                    resultBytes.Data = buffer;
                }
            }
            return resultBytes;
        }

        /// <summary>
        /// 读取服务器的数据，需要指定不同的功能码
        /// </summary>
        /// <param name="address">地址</param>
        /// <param name="length">长度</param>
        /// <returns>带结果信息的字节返回数据</returns>
        protected Result<byte[]> ReadModBusBase( ModbusAddress address, ushort length )
        {
            Result<byte[]> command = BuildReadRegisterCommand( address, length );
            if (!command.Success) return Result.CreateFailedResult<byte[]>( command );

            Result<byte[]> resultBytes = CheckModbusTcpResponse( command.Data );
            if (resultBytes.Success)
            {
                // 二次数据处理
                if (resultBytes.Data.Length >= 3)
                {
                    byte[] buffer = new byte[resultBytes.Data.Length - 3];
                    Array.Copy( resultBytes.Data, 3, buffer, 0, buffer.Length );
                    resultBytes.Data = buffer;
                }
            }
            return resultBytes;
        }
        
        /// <summary>
        /// 读取线圈，需要指定起始地址
        /// </summary>
        /// <param name="address">起始地址，格式为"1234"</param>
        /// <returns>带有成功标志的bool对象</returns>
        public Result<bool> ReadCoil( string address )
        {
            var read = ReadCoil( address, 1 );
            if (!read.Success) return Result.CreateFailedResult<bool>( read );

            return Result.CreateSuccessResult( read.Data[0] );
        }
        
        /// <summary>
        /// 批量的读取线圈，需要指定起始地址，读取长度
        /// </summary>
        /// <param name="address">起始地址，格式为"1234"</param>
        /// <param name="length">读取长度</param>
        /// <returns>带有成功标志的bool数组对象</returns>
        public Result<bool[]> ReadCoil( string address, ushort length )
        {
            var read = ReadModBusBase( ModbusInfo.ReadCoil, address, length );
            if (!read.Success) return Result.CreateFailedResult<bool[]>( read );

            return Result.CreateSuccessResult( Utils.ByteToBoolArray( read.Data, length ) );
        }
        
        /// <summary>
        /// 读取输入线圈，需要指定起始地址
        /// </summary>
        /// <param name="address">起始地址，格式为"1234"</param>
        /// <returns>带有成功标志的bool对象</returns>
        public Result<bool> ReadDiscrete( string address )
        {
            var read = ReadDiscrete( address, 1 );
            if (!read.Success) return Result.CreateFailedResult<bool>( read );

            return Result.CreateSuccessResult( read.Data[0] );
        }
        
        /// <summary>
        /// 批量的读取输入点，需要指定起始地址，读取长度
        /// </summary>
        /// <param name="address">起始地址，格式为"1234"</param>
        /// <param name="length">读取长度</param>
        /// <returns>带有成功标志的bool数组对象</returns>
        public Result<bool[]> ReadDiscrete( string address, ushort length )
        {
            var read = ReadModBusBase( ModbusInfo.ReadDiscrete, address, length );
            if (!read.Success) return Result.CreateFailedResult<bool[]>( read );

            return Result.CreateSuccessResult( Utils.ByteToBoolArray( read.Data, length ) );
        }
        
        /// <summary>
        /// 从Modbus服务器批量读取寄存器的信息，需要指定起始地址，读取长度
        /// </summary>
        /// <param name="address">起始地址，格式为"1234"，或者是带功能码格式x=3;1234</param>
        /// <param name="length">读取的数量</param>
        /// <returns>带有成功标志的字节信息</returns>
        public override Result<byte[]> Read( string address, ushort length )
        {
            Result<ModbusAddress> analysis = ModbusInfo.AnalysisReadAddress( address, isAddressStartWithZero );
            if (!analysis.Success) return Result.CreateFailedResult<byte[]>( analysis );

            List<byte> lists = new List<byte>( );
            ushort alreadyFinished = 0;
            while (alreadyFinished < length)
            {
                ushort lengthTmp = (ushort)Math.Min( (length - alreadyFinished), 120 );
                Result<byte[]> read = ReadModBusBase( analysis.Data.AddressAdd( alreadyFinished ), lengthTmp );
                if (!read.Success) return Result.CreateFailedResult<byte[]>( read );

                lists.AddRange( read.Data );
                alreadyFinished += lengthTmp;
            }
            return Result.CreateSuccessResult( lists.ToArray( ) );
        }
        
        #endregion

        #region Write One Register
        
        /// <summary>
        /// 写一个寄存器数据
        /// </summary>
        /// <param name="address">起始地址</param>
        /// <param name="high">高位</param>
        /// <param name="low">地位</param>
        /// <returns>返回写入结果</returns>
        public Result WriteOneRegister( string address, byte high, byte low )
        {
            Result<byte[]> command = BuildWriteOneRegisterCommand( address, new byte[] { high, low } );
            if (!command.Success) return command;

            return CheckModbusTcpResponse( command.Data );
        }

        /// <summary>
        /// 写一个寄存器数据
        /// </summary>
        /// <param name="address">起始地址</param>
        /// <param name="value">写入值</param>
        /// <returns>返回写入结果</returns>
        public Result WriteOneRegister( string address, short value )
        {
            byte[] buffer = BitConverter.GetBytes( value );
            return WriteOneRegister( address, buffer[1], buffer[0] );
        }

        /// <summary>
        /// 写一个寄存器数据
        /// </summary>
        /// <param name="address">起始地址</param>
        /// <param name="value">写入值</param>
        /// <returns>返回写入结果</returns>
        public Result WriteOneRegister( string address, ushort value )
        {
            byte[] buffer = BitConverter.GetBytes( value );
            return WriteOneRegister( address, buffer[1], buffer[0] );
        }
        
        #endregion

        #region Write Base
        
        /// <summary>
        /// 将数据写入到Modbus的寄存器上去，需要指定起始地址和数据内容
        /// </summary>
        /// <param name="address">起始地址，格式为"1234"</param>
        /// <param name="value">写入的数据，长度根据data的长度来指示</param>
        /// <returns>返回写入结果</returns>
        /// <remarks>
        /// 富地址格式，支持携带站号信息，功能码信息，具体参照类的示例代码
        /// </remarks>
        public override Result Write( string address, byte[] value )
        {
            // 解析指令
            Result<byte[]> command = BuildWriteRegisterCommand( address, value );
            if (!command.Success) return command;

            // 核心交互
            return CheckModbusTcpResponse( command.Data );
        }
        
        #endregion

        #region Write Coil

        /// <summary>
        /// 写一个线圈信息，指定是否通断
        /// </summary>
        /// <param name="address">起始地址</param>
        /// <param name="value">写入值</param>
        /// <returns>返回写入结果</returns>
        public Result WriteCoil( string address, bool value )
        {
            // 解析指令
            Result<byte[]> command = BuildWriteOneCoilCommand( address, value );
            if (!command.Success) return command;

            // 核心交互
            return CheckModbusTcpResponse( command.Data );
        }

        /// <summary>
        /// 批量写入线圈信息，指定是否通断
        /// </summary>
        /// <param name="address">起始地址</param>
        /// <param name="values">写入值</param>
        /// <returns>返回写入结果</returns>
        public Result WriteCoil( string address, bool[] values )
        {
            // 解析指令
            Result<byte[]> command = BuildWriteCoilCommand( address, values );
            if (!command.Success)  return command;

            // 核心交互
            return CheckModbusTcpResponse( command.Data );
        }
        
        #endregion

        #region Write bool[]

        /// <summary>
        /// 向寄存器中写入bool数组，返回值说明，比如你写入M100,那么data[0]对应M100.0
        /// </summary>
        /// <param name="address">要写入的数据地址</param>
        /// <param name="values">要写入的实际数据，长度为8的倍数</param>
        /// <returns>返回写入结果</returns>
        public Result Write( string address, bool[] values )
        {
            return Write( address, Utils.BoolArrayToByte( values ) );
        }
        
        #endregion
        
        #region Object Override

        /// <summary>
        /// 返回表示当前对象的字符串
        /// </summary>
        /// <returns>字符串信息</returns>
        public override string ToString( )
        {
            return "ModbusRtu";
        }

        #endregion
        
    }
}
