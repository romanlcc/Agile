﻿namespace Microsoft.Extensions.HealthChecks
{
    public class HealthCheckResult
    {
        public CheckStatus CheckStatus { get; }
        public string Description { get; }
        public string Name { get; }
        public string Duration { get; }
        private HealthCheckResult(CheckStatus checkStatus, string description, string name, string duration)
        {
            CheckStatus = checkStatus;
            Description = description;
            Name = name;
            Duration = duration;
        }
        public static HealthCheckResult Unhealthy(string name, string description, string duration)
        {
            return new HealthCheckResult(CheckStatus.Unhealthy, description, name, duration);
        }
        public static HealthCheckResult Healthy(string name, string description, string duration)
        {
            return new HealthCheckResult(CheckStatus.Healthy, description, name, duration);
        }
        public static HealthCheckResult Warning(string name, string description, string duration)
        {
            return new HealthCheckResult(CheckStatus.Warning, description, name, duration);
        }
        public static HealthCheckResult Unknown(string name, string description, string duration)
        {
            return new HealthCheckResult(CheckStatus.Unknown, description, name, duration);
        }
    }
}
