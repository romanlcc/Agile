﻿namespace Microsoft.Extensions.HealthChecks
{
    public enum CheckStatus : int
    {
        Healthy = 1,
        Unhealthy = 2,
        Warning = 3,
        Unknown = 4,
      
    }
}
