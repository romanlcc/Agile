using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;

using MySql.Data.MySqlClient;

namespace Microsoft.Extensions.HealthChecks
{
    public static class HealthCheckBuilderExtensions
    {
        public static HealthCheckBuilder AddUrlChecks(this HealthCheckBuilder builder, IEnumerable<string> urlItems, string group)
        {
            var urls = urlItems.ToList();
            builder.AddCheck($"UrlChecks ({group})", async () =>
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();

                #region ==ҵ��==
                var successfulChecks = 0;
                var description = new StringBuilder();
                var httpClient = new HttpClient();
                foreach (var url in urlItems)
                {
                    try
                    {
                        //httpClient.DefaultRequestHeaders.Add("cache-control", "no-cache");
                        var response = await httpClient.GetAsync(url);

                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            successfulChecks++;
                            description.Append($"UrlCheck SUCCESS ({url}) ");
                        }
                        else
                        {
                            description.Append($"UrlCheck FAILED ({url}) ");
                        }
                    }
                    catch
                    {
                        description.Append($"UrlCheck FAILED ({url}) ");
                    }
                }
                #endregion

                sw.Stop();
                TimeSpan ts = sw.Elapsed;

                if (successfulChecks == urls.Count)
                {
                    return HealthCheckResult.Healthy("UrlChecks", description.ToString(), ts.TotalMilliseconds.ToString());
                }
                else if (successfulChecks > 0)
                {
                    return HealthCheckResult.Warning("UrlChecks", description.ToString(), ts.TotalMilliseconds.ToString());
                }
                return HealthCheckResult.Unhealthy("UrlChecks", description.ToString(), ts.TotalMilliseconds.ToString());

            });
            return builder;
        }
        public static HealthCheckBuilder AddVirtualMemorySizeCheck(this HealthCheckBuilder builder, long maxSize)
        {
            builder.AddCheck($"VirtualMemorySize ({maxSize})", () =>
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();

                var flg = Process.GetCurrentProcess().VirtualMemorySize64 <= maxSize;

                sw.Stop();
                TimeSpan ts = sw.Elapsed;

                if (flg)
                {
                    return HealthCheckResult.Healthy("VirtualMemorySize", $"AddVirtualMemorySizeCheck, maxSize: {maxSize}", ts.TotalMilliseconds.ToString());
                }
                return HealthCheckResult.Unhealthy("VirtualMemorySize", $"AddVirtualMemorySizeCheck, maxSize: {maxSize}", ts.TotalMilliseconds.ToString());
            });

            return builder;
        }

        public static HealthCheckBuilder AddWorkingSetCheck(this HealthCheckBuilder builder, long maxSize)
        {
            builder.AddCheck($"WorkingSet64 ({maxSize})", () =>
            {

                Stopwatch sw = new Stopwatch();
                sw.Start();

                var flg = Process.GetCurrentProcess().WorkingSet64 <= maxSize;

                sw.Stop();
                TimeSpan ts = sw.Elapsed;

                if (flg)
                {
                    return HealthCheckResult.Healthy("WorkingSet64", $"AddWorkingSetCheck, maxSize: {maxSize}", ts.TotalMilliseconds.ToString());
                }
                return HealthCheckResult.Unhealthy("WorkingSet64", $"AddWorkingSetCheck, maxSize: {maxSize}", ts.TotalMilliseconds.ToString());


            });

            return builder;
        }

        public static HealthCheckBuilder AddPrivateMemorySizeCheck(this HealthCheckBuilder builder, long maxSize)
        {
            builder.AddCheck($"PrivateMemorySize64 ({maxSize})", () =>
            {

                Stopwatch sw = new Stopwatch();
                sw.Start();

                var flg = Process.GetCurrentProcess().PrivateMemorySize64 <= maxSize;

                sw.Stop();
                TimeSpan ts = sw.Elapsed;

                if (flg)
                {
                    return HealthCheckResult.Healthy("PrivateMemorySize64", $"AddPrivateMemorySizeCheck, maxSize: {maxSize}", ts.TotalMilliseconds.ToString());
                }
                return HealthCheckResult.Unhealthy("PrivateMemorySize64", $"AddPrivateMemorySizeCheck, maxSize: {maxSize}", ts.TotalMilliseconds.ToString());

            });

            return builder;
        }
        public static HealthCheckBuilder AddDbCheck(this HealthCheckBuilder builder)
        {
            builder.AddCheck($"Db Check:", async () =>
            {
                try
                {
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    int result = 0;
                    var dbOptions = new ConfigurationHelper().Get<DbOptions>("Db", "");
                    switch (dbOptions.DbType)
                    {
                        case DatabaseType.SqlServer:
                            using (var connection = new SqlConnection(dbOptions.ConnectionString))
                            {
                                connection.Open();
                                using (var command = connection.CreateCommand())
                                {
                                    command.CommandType = CommandType.Text;
                                    command.CommandText = "SELECT 1";
                                    result = (int)await command.ExecuteScalarAsync();
                                }
                            }
                            break;
                        case DatabaseType.MySql:
                            using (var connection = new MySqlConnection(dbOptions.ConnectionString))
                            {
                                connection.Open();
                                using (var command = connection.CreateCommand())
                                {
                                    command.CommandType = CommandType.Text;
                                    command.CommandText = "SELECT 1";
                                    result = (int)await command.ExecuteScalarAsync();
                                }
                            }
                            break;
                        case DatabaseType.Oracle:
                            using (var connection = new OracleConnection(dbOptions.ConnectionString))
                            {
                                connection.Open();
                                using (var command = connection.CreateCommand())
                                {
                                    command.CommandType = CommandType.Text;
                                    command.CommandText = "SELECT 1";
                                    result = (int)await command.ExecuteScalarAsync();
                                }
                            }
                            break;
                        case DatabaseType.Sqlite:
                            using (var connection = new SQLiteConnection(dbOptions.ConnectionString))
                            {
                                connection.Open();
                                using (var command = connection.CreateCommand())
                                {
                                    command.CommandType = CommandType.Text;
                                    command.CommandText = "SELECT 1";
                                    result = (int)await command.ExecuteScalarAsync();
                                }
                            }
                            break;
                        default:
                            break;
                    }

                    sw.Stop();
                    TimeSpan ts = sw.Elapsed;

                    if (result == 1)
                    {
                        return HealthCheckResult.Healthy("Db", $"{dbOptions.ToString()}", ts.TotalMilliseconds.ToString());
                    }
                    return HealthCheckResult.Unhealthy("Db", $"{dbOptions.ToString()}", ts.TotalMilliseconds.ToString());
                }
                catch (Exception ex)
                {
                    return HealthCheckResult.Unhealthy("Db", ex.Message, "");
                }
            });

            return builder;

        }

        public static HealthCheckBuilder AddEnvironmentCheck(this HealthCheckBuilder builder)
        {
            builder.AddCheck($"Environment Check:", () =>
            {
                Dictionary<string, object> dictionary = new Dictionary<string, object>
                {
                    {
                        "MachineName",
                        Environment.MachineName
                    }
                };
                try
                {
                    Stopwatch sw = new Stopwatch();
                    sw.Start();

                    #region ==ҵ��==
                    var environmentVariables = Environment.GetEnvironmentVariables();
                    foreach (object key in environmentVariables.Keys)
                    {
                        if (key != null)
                        {
                            string text = key as string;
                            object obj = environmentVariables[key];
                            if (text != null && obj != null)
                            {
                                dictionary.Add(text, obj);
                            }
                        }
                    }
                    #endregion

                    sw.Stop();
                    TimeSpan ts = sw.Elapsed;

                    return HealthCheckResult.Healthy("Environment", $"{dictionary.ToJson()}", ts.TotalMilliseconds.ToString());

                }
                catch
                {
                    return HealthCheckResult.Unhealthy("Environment", $"{dictionary.ToJson()}", "");
                }
            });

            return builder;
        }

        public static HealthCheckBuilder AddGcCheck(this HealthCheckBuilder builder)
        {
            builder.AddCheck($"GC Check:", () =>
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();

                long totalMemory = GC.GetTotalMemory(forceFullCollection: false);
                Dictionary<string, object> data = new Dictionary<string, object>
                {
                    {
                        "AllocatedMBytes",
                        totalMemory / 1024 / 1024
                    },
                    {
                        "Gen0Collections",
                        GC.CollectionCount(0)
                    },
                    {
                        "Gen1Collections",
                        GC.CollectionCount(1)
                    },
                    {
                        "Gen2Collections",
                        GC.CollectionCount(2)
                    }
                };

                sw.Stop();
                TimeSpan ts = sw.Elapsed;

                return HealthCheckResult.Healthy("GC", $"AddGcCheck:{data.ToJson()}", ts.TotalMilliseconds.ToString());
            });

            return builder;
        }

        public static HealthCheckBuilder AddMemoryCheck(this HealthCheckBuilder builder)
        {
            builder.AddCheck($"Memory Check:", () =>
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();

                Process currentProcess = Process.GetCurrentProcess();
                Dictionary<string, object> data = new Dictionary<string, object>
                {
                    {
                        "Id",
                        currentProcess.Id
                    },
                    {
                        "WorkingSet",
                        currentProcess.WorkingSet64
                    },
                    {
                        "PrivateMemory",
                        currentProcess.PrivateMemorySize64
                    },
                    {
                        "VirtualMemory",
                        currentProcess.VirtualMemorySize64
                    }
                };

                sw.Stop();
                TimeSpan ts = sw.Elapsed;

                return HealthCheckResult.Healthy("Memory", $"AddMemoryCheck:{data.ToJson()}", ts.TotalMilliseconds.ToString());
            });

            return builder;
        }

        public static HealthCheckBuilder AddNetCoreRuntimeCheck(this HealthCheckBuilder builder)
        {
            builder.AddCheck($"NetCoreRuntime Check:", async () =>
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();

                Process process = Process.Start(new ProcessStartInfo("dotnet", "--info")
                {
                    UseShellExecute = false,
                    RedirectStandardOutput = true
                });
                process.WaitForExit(5000);
                string value = await process.StandardOutput.ReadToEndAsync();
                Dictionary<string, object> data = new Dictionary<string, object>
                {
                    {
                        "dotnet --info",
                        value
                    }
                };

                sw.Stop();
                TimeSpan ts = sw.Elapsed;

                return HealthCheckResult.Healthy("NetCoreRuntime", $"AddNetCoreRuntimeCheck:{data.ToJson()}", ts.TotalMilliseconds.ToString());
            });

            return builder;
        }
    }
}