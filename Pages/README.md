
Agile就是dotnet core 基础类库 Auth,Entity,Extention,Helper,Mvc,Log,Infrastructure,Specifications,Dapper,ModBus,PLC,Socket,Tencent,Weixin等DotNet Core基础类大全
## 特性

1. Auth  
1. Entity  
1. Extention Log 
1. Helper  
1. Infrastructure  
1. Specifications  
1. Dapper  
1. ModBus,PLC,Socket  
1. Mvc  Tencent Weixin

## 技术栈
* 后端 .net core + Dapper + Extention + ModBus
* 代码生成工具 CodeSmith
* 设计工具 PowerDesigner + Enterprise Architect
* 部署环境 docker + jenkins

## 系统工程结构：
1. Agile 通用工具集合
1. Agile.Data 系统仓储层，用于数据库操作
1. Agile.HealthChecks 通用工具集合
1. Agile.NetWork 通用工具集合
1. Agile.Tencent 通用工具集合

## 使用

从[快速开始](core_start.md)开始你的神奇之旅吧！


