## 下载代码

使用git工具下载代码，代码地址：https://gitee.com/luomingui/Agile

## 安装sdk

下载安装微软官方SDK，代码地址：https://dotnet.microsoft.com/download


## 打开项目

使用vs2019(推荐)打开 `Agile.sln`


## 编译运行

使用visualstudio生成解决方案。
`注：首次启动时，visual studio会启动nuget还原第三方依赖包，请保持网络通畅，并等待一段时间`



